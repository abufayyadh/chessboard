const makeChessboard = () => {
  let chessboard = [];

  let row0 = [
    "B Black",
    "K Black",
    "M Black",
    "RT Black",
    "RJ Black",
    "M Black",
    "K Black",
    "B Black",
  ];
  let row1 = [
    "P Black",
    "P Black",
    "P Black",
    "P Black",
    "P Black",
    "P Black",
    "P Black",
    "P Black",
  ];

  let row6 = [
    "P White",
    "P White",
    "P White",
    "P White",
    "P White",
    "P White",
    "P White",
    "P White",
  ];
  let row7 = [
    "B White",
    "K White",
    "M White",
    "RT White",
    "RJ White",
    "M White",
    "K White",
    "B White",
  ];

  // console.log(row0);
  // console.log(row1);
  // console.log(row6);
  // console.log(row7);

  let emptyRow = [];

  for (let i = 0; i < 8; i++) {
    emptyRow.push("-");
  }

  chessboard.push(row0);
  chessboard.push(row1);
  chessboard.push(emptyRow);
  chessboard.push(emptyRow);
  chessboard.push(emptyRow);
  chessboard.push(emptyRow);
  chessboard.push(row6);
  chessboard.push(row7);

  return chessboard;
};

const printBoard = (x) => {
  for (let i = 0; i < 8; i++) {
    console.log(x[i]);
    // console.log(x[1][5]);
  }
};

printBoard(makeChessboard());
